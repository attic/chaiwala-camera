/* vim: set sts=4 sw=4 et :
 *
 * License: LGPL-2
 *
 * Take YUYV input from the specified v4l camera, convert to BGR32, and write to
 * the specified framebuffer.
 *
 * Currently only supports YUYV (most webcams) -> BGR32 (most KMS drivers) conversion.
 *
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/time.h>

#include <linux/kd.h>
#include <linux/fb.h>
#include <linux/videodev2.h>

#define ARRAY_LENGTH(a) (sizeof(a) / sizeof((a)[0]))
#define V4L_TEST "v4l_test"
#define N_BUFFERS 4

static bool testing_mode = false;
static unsigned long n_frames = ULONG_MAX;
static const char *v4l_device;
static const char *fb_device;
/* Maximum time in milliseconds to retry any single operation before failing */
static const time_t hangcheck_ms = 5000;

struct chaiwala_buffer {
    unsigned char *data;
    size_t length;
};

struct chaiwala_v4l_fb_context {
    int fb_fd;
    int v4l_fd;
    struct v4l2_format src_fmt;
    struct v4l2_format dest_fmt;
    struct chaiwala_buffer src_buf[N_BUFFERS];
    struct chaiwala_buffer dest_buf;
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
};

/** START: Copied from libav and modified **/
struct rgb_pixfmt_map_entry {
    int bits_per_pixel;
    int red_offset, green_offset, blue_offset, alpha_offset;
    uint32_t pixfmt;
};

static struct rgb_pixfmt_map_entry rgb_pixfmt_map[] = {
    // bpp, red_offset,  green_offset, blue_offset, alpha_offset, pixfmt
    {  32,       0,           8,          16,           24,   V4L2_PIX_FMT_RGB32   },
    {  32,      16,           8,           0,           24,   V4L2_PIX_FMT_BGR32   },
    {  24,       0,           8,          16,            0,   V4L2_PIX_FMT_RGB24   },
    {  24,      16,           8,           0,            0,   V4L2_PIX_FMT_BGR24   },
    {  16,      11,           5,           0,            0,   V4L2_PIX_FMT_RGB565X },
};

static uint32_t
get_pixfmt_from_fb_varinfo (struct fb_var_screeninfo *vinfo)
{
    int i;

    for (i = 0; i < ARRAY_LENGTH(rgb_pixfmt_map); i++) {
        struct rgb_pixfmt_map_entry *entry = &rgb_pixfmt_map[i];
        if (entry->bits_per_pixel == vinfo->bits_per_pixel &&
            entry->red_offset     == vinfo->red.offset     &&
            entry->green_offset   == vinfo->green.offset   &&
            entry->blue_offset    == vinfo->blue.offset)
            return entry->pixfmt;
    }

    fprintf (stderr, "Unsupported pixfmt from vinfo:\n"
             "bpp: %i, red: %i, green: %i, blue: %i, alpha: %i\n",
             vinfo->bits_per_pixel, vinfo->red.offset,
             vinfo->green.offset, vinfo->blue.offset,
             vinfo->transp.offset);
    return 0;
}
/** END: Copied from libav and modified **/

static const char*
pixfmt_to_string (uint32_t pixfmt)
{
    switch (pixfmt) {
        case V4L2_PIX_FMT_RGB32:
            return "V4L2_PIX_FMT_RGB32";
        case V4L2_PIX_FMT_BGR32:
            return "V4L2_PIX_FMT_BGR32";
        case V4L2_PIX_FMT_RGB24:
            return "V4L2_PIX_FMT_RGB24";
        case V4L2_PIX_FMT_BGR24:
            return "V4L2_PIX_FMT_BGR24";
        case V4L2_PIX_FMT_YUYV:
            return "V4L2_PIX_FMT_YUYV";
        case V4L2_PIX_FMT_MJPEG:
            return "V4L2_PIX_FMT_MJPEG";
        case V4L2_PIX_FMT_RGB565X:
            return "V4L2_PIX_FMT_RGB565X";
    }
    return "Unknown pixel format";
}

static int
ioctl_retry (int fd, int req, void *arg)
{
    int r;

    /* TODO: Add hangcheck support */
    do {
        r = ioctl (fd, req, arg);
    } while (r < 0 && errno == EINTR);

    return r;
}

#if 0
// https://github.com/desowin/v4l-examples/blob/master/sdlvideoviewer-rgb565x.c#L252
static uint16_t inline
RGB888_to_RGB565X (uint32_t rgb)
{
    uint16_t tmp;

    tmp = ((rgb >> 3) & 0x1F) << 11 | /* Blue */
          ((rgb >> 10) & 0x3F) << 5 | /* Green */
          ((rgb >> 19) & 0x1F); /* Red */

#if __BYTE_ORDER == __LITTLE_ENDIAN
        /* In LE lower byte is stored under lower address */
        tmp = ((tmp >> 8) & 0xFF) | ((tmp & 0xFF) << 8);
#elif __BYTE_ORDER == __BIG_ENDIAN
            /* Nothing to do */
#else
#error "Unknown Endianess"
#endif

    return tmp;
}
#endif

static size_t
write_yuyv_to_bgr32_to_fb (const unsigned char *src,
                           size_t src_size,
                           unsigned char *dest,
                           struct chaiwala_v4l_fb_context *ctx)
{
    int in;
    int out_pixels;
    int bytes_per_pixel;
    int fb_visible_width, fb_width, fb_height;
    int v4l_width, v4l_height;

    bytes_per_pixel = ctx->vinfo.bits_per_pixel >> 3;

    if (!testing_mode) {
        /* At least on KMS drivers, the visible width (x res) is different from
         * the in-memory line width (line_length) */
        fb_visible_width = ctx->vinfo.xres;
        fb_width = ctx->finfo.line_length / bytes_per_pixel;
        fb_height = ctx->vinfo.yres;
        v4l_width = ctx->src_fmt.fmt.pix.width;
        v4l_height = ctx->src_fmt.fmt.pix.height;

        /* Center video vertically */
        dest += ((fb_height - v4l_height) * fb_width) * (bytes_per_pixel >> 1);
        /* Center video horizontally */
        dest += (fb_visible_width - v4l_width) * (bytes_per_pixel >> 1);
    }

#define CLIP(color) (unsigned char)(((color) > 0xFF) ? 0xff : (((color) < 0) ? 0 : (color)))
    for (in = 0, out_pixels = 0; in < src_size; in += 4) {
        /* YUYV -> BGR32, copied from libv4lconvert and modified */
        int u = src[1];
        int v = src[3];
        int u1 = (((u - 128) << 7) +  (u - 128)) >> 6;
        int rg = (((u - 128) << 1) +  (u - 128) +
                  ((v - 128) << 2) +  ((v - 128) << 1)) >> 3;
        int v1 = (((v - 128) << 1) +  (v - 128)) >> 1;

        *dest++ = CLIP(src[0] + u1);
        *dest++ = CLIP(src[0] - rg);
        *dest++ = CLIP(src[0] + v1);
        *dest++ = 0;

        *dest++ = CLIP(src[2] + u1);
        *dest++ = CLIP(src[2] - rg);
        *dest++ = CLIP(src[2] + v1);
        *dest++ = 0;

        src += 4;
        out_pixels += 2;
        if (!testing_mode && !(out_pixels % v4l_width))
            /* Skip till start of next line of the FB */
            dest += (fb_width - v4l_width) * bytes_per_pixel;
    }
    return out_pixels * bytes_per_pixel;
}

static int
process_buffer (struct chaiwala_v4l_fb_context *ctx)
{
    size_t written;
    struct v4l2_buffer buf;

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    fprintf (stderr, ".");
    if (ioctl_retry (ctx->v4l_fd, VIDIOC_DQBUF, &buf) < 0) {
        switch (errno) {
            case EAGAIN:
                fprintf (stderr, "Got EGAIN\n");
                return 0;
            case EIO:
                /* Could ignore EIO, see:
                 * http://linuxtv.org/downloads/v4l-dvb-apis/vidioc-qbuf.html */
            default:
                fprintf (stderr, "Unable to get frame from v4l: %i, %s\n",
                         errno, strerror (errno));
                return -1;
        }
    }

    written = write_yuyv_to_bgr32_to_fb (ctx->src_buf[buf.index].data,
                                         buf.bytesused,
                                         ctx->dest_buf.data,
                                         ctx);

    if (testing_mode) {
        (void) fwrite (ctx->dest_buf.data,
                       written, 1, stdout);
        fflush (stdout);
    }

    /* Queue the buffer back into capture device to get a later frame */
    if (ioctl_retry (ctx->v4l_fd, VIDIOC_QBUF, &buf) < 0) {
        fprintf (stderr, "Unable to queue buffer into v4l device: %i, %s\n",
                 errno, strerror (errno));
        return -2;
    }

    return 0;
}

static int
mainloop (struct chaiwala_v4l_fb_context *ctx)
{
    int r;

    while (n_frames--) {
        fd_set read_fds;
        struct timeval timeout;

        FD_ZERO (&read_fds);
        FD_SET (ctx->v4l_fd, &read_fds);

        /* Timeout. */
        timeout.tv_sec = hangcheck_ms / 1000;
        timeout.tv_usec = 0;

        r = select (ctx->v4l_fd + 1, &read_fds, NULL, NULL, &timeout);

        if (r < 0) {
            if (EINTR == errno)
                continue;
            fprintf (stderr, "select error: %i, %s\n",
                     errno, strerror (errno));
        }

        if (r == 0) {
            fprintf (stderr, "select timeout\n");
            continue;
        }

        if (process_buffer (ctx) < 0)
            break;
        /* EAGAIN - continue select loop. */
    }
    return 0;
}

static int
start_capturing (struct chaiwala_v4l_fb_context *ctx)
{
    int index;
    struct v4l2_buffer buf;
    struct v4l2_requestbuffers req;
    enum v4l2_buf_type type;

    /* Request device to mmap() its memory for us. */
    req.count = N_BUFFERS;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (ioctl_retry (ctx->v4l_fd, VIDIOC_REQBUFS, &req) < 0) {
        if (errno == EINVAL) {
            fprintf (stderr, "Device does not support mmap()ed memory: %i, %s\n",
                     errno, strerror (errno));
            return -10;
        } else {
            fprintf (stderr, "Unable to request v4l buffers: %i, %s\n",
                     errno, strerror (errno));
            return -20;
        }
    }

    if (req.count < N_BUFFERS) {
        fprintf (stderr, "Unable to allocate sufficient no. of buffers: %i, %s",
                 errno, strerror (errno));
        return -30;
    }

    /* Get a pointer to the mmap()ed memory.
     * Since we asked for just one buffer, we only need to call this once with
     * buf.index = 0*/
    for (index = 0; index < req.count; ++index) {
        buf.index = index;
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        if (ioctl_retry (ctx->v4l_fd, VIDIOC_QUERYBUF, &buf) < 0) {
            fprintf (stderr, "Unable to query buffer from v4l device: %i, %s\n",
                     errno, strerror (errno));
            return -40;
        }

        ctx->src_buf[index].length = buf.length;
        ctx->src_buf[index].data = mmap (NULL, ctx->src_buf[index].length,
                                  PROT_READ | PROT_WRITE, MAP_SHARED,
                                  ctx->v4l_fd, buf.m.offset);
        if (ctx->src_buf[index].data == MAP_FAILED) {
            fprintf (stderr, "Unable to mmap v4l buffers: %i, %s\n",
                     errno, strerror (errno));
            return -50;
        }

        /* Queue each buffer into capture device to get the first frames */
        if (ioctl_retry (ctx->v4l_fd, VIDIOC_QBUF, &buf) < 0) {
            fprintf (stderr, "Unable to queue buffer into v4l device: %i, %s\n",
                     errno, strerror (errno));
            return -60;
        }
    }

    /* Start streaming. */
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl_retry (ctx->v4l_fd, VIDIOC_STREAMON, &type) < 0) {
        fprintf (stderr, "Unable to begin v4l capture: %i, %s\n",
                 errno, strerror (errno));
        return -70;
    }

#if 0
    fprintf (stderr, "src_buf.length: %zi\n"
            "buf.bytesused: %u\n"
            "dest_buf.length: %zi\n"
            "src_fmt: %s\n"
            "dest_fmt: %s\n"
            "src sizeimage: %i\n",
            ctx->src_buf[buf.index].length,
            buf.bytesused,
            ctx->dest_buf.length,
            pixfmt_to_string (ctx->src_fmt.fmt.pix.pixelformat),
            pixfmt_to_string (ctx->dest_fmt.fmt.pix.pixelformat),
            ctx->src_fmt.fmt.pix.sizeimage);
#endif

    return 0;
}

static int
init_v4l_device (struct chaiwala_v4l_fb_context *ctx)
{
    struct v4l2_capability cap;

    /* Verify that the device supports video capture streaming */
    if (ioctl_retry (ctx->v4l_fd, VIDIOC_QUERYCAP, &cap) < 0) {
        if (errno == EINVAL) {
            fprintf (stderr, "'%s' is not a v4l device\n", v4l_device);
            return -1;
        } else {
            fprintf (stderr, "%s error: %i, %s (not a v4l device?)\n",
                     "VIDIOC_QUERYCAP", errno, strerror (errno));
            return -1;
        }
    }
    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
        fprintf (stderr, "%s does not support streaming i/o\n", v4l_device);
        return -1;
    }

    /* To avoid having to use a decoder (MJPEG), we ask for YUV.
     * To see the options for your capture device, run: 
     *  `v4l2-ctl --list-formats-ext`
     *
     * We ask for the screen resolution, and the driver gives us something close
     * to it. */
    ctx->src_fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    ctx->src_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    /* Do we care whether it's progressive or not? */
    ctx->src_fmt.fmt.pix.field = V4L2_FIELD_NONE;
    if (!testing_mode) {
        ctx->src_fmt.fmt.pix.width = ctx->vinfo.xres;
        ctx->src_fmt.fmt.pix.height = ctx->vinfo.yres;
    } else {
        ctx->src_fmt.fmt.pix.width = 640;
        ctx->src_fmt.fmt.pix.height = 480;
    }

    /* Find out what kind of formats are supported by the capture device
     * This will change src_fmt for us to something close to what we need */
    if (ioctl_retry (ctx->v4l_fd, VIDIOC_TRY_FMT, &ctx->src_fmt) < 0) {
        fprintf (stderr, "Format '%s' unsupported by v4l device. Aborting.\n",
                 pixfmt_to_string (ctx->src_fmt.fmt.pix.pixelformat));
        return -1;
    }

    if (ctx->src_fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_YUYV) {
        fprintf (stderr, "V4L device supports %i which is not YUYV; aborting\n",
                 ctx->src_fmt.fmt.pix.pixelformat);
        return -1;
    }

    /* Set the supported output format we got back from the driver */
    if (ioctl_retry (ctx->v4l_fd, VIDIOC_S_FMT, &ctx->src_fmt) < 0) {
        fprintf (stderr, "Unable to set v4l device output format: %i, %s\n",
                 errno, strerror (errno));
        return -1;
    }

    /* Copy all format attributes, and change the pixel format for writing to
     * the framebuffer.  */
    ctx->dest_fmt = ctx->src_fmt;

    if (!testing_mode) {
        /* Get dest pixel format from FB, and check if it's supported */
        ctx->dest_fmt.fmt.pix.pixelformat = get_pixfmt_from_fb_varinfo (&ctx->vinfo);
        if (ctx->dest_fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_BGR32) {
            fprintf (stderr, "Unsupported framebuffer pixel format -- we only support V4L2_PIX_FMT_BGR32\n");
            return -1;
        }

        ctx->dest_buf.length = ctx->finfo.line_length * ctx->vinfo.yres;
        ctx->dest_buf.data = mmap(0, ctx->dest_buf.length,
                                  PROT_READ | PROT_WRITE, MAP_SHARED,
                                  ctx->fb_fd, 0);
        if (ctx->dest_buf.data == MAP_FAILED) {
            fprintf (stderr, "Unable to map framebuffer: %i, %s\n",
                     errno, strerror (errno));
            return -1;
        }
    } else {
        /* While testing, we use BGR32 everywhere */
        ctx->dest_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_BGR32;
        ctx->dest_buf.length = ctx->dest_fmt.fmt.pix.width *
                               ctx->dest_fmt.fmt.pix.height *
                               32 / 8;
        ctx->dest_buf.data = malloc (ctx->dest_buf.length);
    }

    return 0;
}

static bool
fetch_fscreeninfo (int fb_fd,
                   struct fb_fix_screeninfo *finfo)
{
    if (ioctl_retry (fb_fd, FBIOGET_FSCREENINFO, finfo) < 0) {
        fprintf (stderr, "Error reading fixed FB screen information: %i, %s\n",
                 errno, strerror (errno));
        return false;
    }
    fprintf(stderr, "Fixed screen info:\n\tFB_TYPE: %u, FB_VISUAL: %u, FB_CAP: %u, line length: %u\n",
           finfo->type, finfo->visual, finfo->capabilities, finfo->line_length);
    return true;
}

static bool
fetch_vscreeninfo (int fb_fd,
                   struct fb_var_screeninfo *vinfo)
{
    if (ioctl_retry (fb_fd, FBIOGET_VSCREENINFO, vinfo) < 0) {
        fprintf (stderr, "Error reading variable FB screen information: %i, %s\n",
                 errno, strerror (errno));
        return false;
    }
    fprintf(stderr, "Variable screen info:\n"
            "\tRes: %ux%u (virt: %ux%u), %ubpp, %s\n", 
            vinfo->xres, vinfo->yres,
            vinfo->xres_virtual, vinfo->yres_virtual,
            vinfo->bits_per_pixel,
            pixfmt_to_string (get_pixfmt_from_fb_varinfo (vinfo)));
    return true;
}

/* Open device for v4l operations (reading and writing) */
static int
open_device (const char *dev_name)
{
    int fd;
    struct stat st;

    if (stat (dev_name, &st) < 0) {
        fprintf (stderr, "Unable to stat '%s': %i, %s\n",
                 dev_name, errno, strerror (errno));
        return -1;
    }

    if (!S_ISCHR (st.st_mode)) {
        fprintf (stderr, "'%s' is not a device\n", dev_name);
        return -2;
    }

    fd = open (dev_name, O_RDWR | O_NONBLOCK, 0);

    if (fd < 0) {
        fprintf (stderr, "Unable to open device '%s': %i, %s\n",
                 dev_name, errno, strerror (errno));
        return -3;
    }

    return fd;
}

int
main (int   argc,
      char *argv[])
{
    int vt_fd;
    struct chaiwala_v4l_fb_context *ctx;

    if (argc < 3) {
        printf ("Usage: %s <v4l_device> <fb_device> [no. of frames]\n"
                "If <fb_device> is '" V4L_TEST "', the v4l output "
                "will be written to stdout as BGR32\n", argv[0]);
        exit (EXIT_FAILURE);
    }

    v4l_device = argv[1];
    fb_device = argv[2];

    if (strcmp (V4L_TEST, fb_device) == 0)
        testing_mode = true;

    if (argc > 3) {
        errno = 0;
        n_frames = strtoul (argv[3], NULL, 10);
        if (errno) {
            fprintf (stderr, "Invalid frame count '%s': %i, %s",
                     argv[3], errno, strerror (errno));
            exit (EXIT_FAILURE);
        }
    }

    ctx = malloc (sizeof(*ctx));

    if (!testing_mode) {
        ctx->fb_fd = open_device (fb_device);
        if (ctx->fb_fd < 0)
            exit (EXIT_FAILURE);
        fprintf (stderr, "Framebuffer device (%s) was opened successfully\n",
                fb_device);
    } else {
        ctx->fb_fd = -1;
        fprintf (stderr, "V4L testing mode. Skipping framebuffer codepaths.\n");
    }

    /* Open webcam */
    ctx->v4l_fd = open_device (v4l_device);
    if (ctx->v4l_fd < 0)
        exit (EXIT_FAILURE);
    fprintf (stderr, "Capture device (%s) was opened successfully\n",
             v4l_device);

    if (!testing_mode) {
        /* Get framebuffer properties */
        if (!fetch_vscreeninfo (ctx->fb_fd, &ctx->vinfo))
            exit (EXIT_FAILURE);
        if (!fetch_fscreeninfo (ctx->fb_fd, &ctx->finfo))
            exit (EXIT_FAILURE);
    } else {
        /* We don't really have a screen, so we fake it by manually setting
         * whatever we will use in testing mode */
        ctx->vinfo.bits_per_pixel = 32;
    }

    if (init_v4l_device (ctx) < 0)
        exit (EXIT_FAILURE);

    fprintf (stderr, "V4L device will give us:\n"
            "\t%ix%i, %s\n",
             ctx->src_fmt.fmt.pix.width,
             ctx->src_fmt.fmt.pix.height,
             pixfmt_to_string (ctx->src_fmt.fmt.pix.pixelformat));

    fprintf (stderr, "Setting tty0 KDMODE to KD_GRAPHICS\n");
    vt_fd = open("/dev/tty0", O_RDWR);
    if (!vt_fd)
        fprintf (stderr, "Unable to open /dev/tty0\n");
    else
        if (ioctl(vt_fd, KDSETMODE, KD_GRAPHICS) < 0)
            fprintf (stderr, "Unable to KDSETMODE, KD_GRAPHICS\n");

    fprintf (stderr, "Starting capture\n");
    if (start_capturing (ctx) < 0)
        exit (EXIT_SUCCESS);

    fprintf (stderr, "Looping...");
    mainloop (ctx);

    exit (EXIT_SUCCESS);
}
