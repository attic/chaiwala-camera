CC		?= gcc
INSTALL		?= install

CFLAGS		?= -Wall -O3 -ffast-math -march=native -fomit-frame-pointer
OUR_CFLAGS	:= $(CFLAGS) -fPIC -std=gnu99

# Paths
PREFIX		?= /usr
SYSCONFDIR	?= /etc
BINDIR		?= $(PREFIX)/bin
LIBDIR		?= $(PREFIX)/lib
DESTDIR		?= 

PROGS		:= chaiwala-v4l2fb
ARCH := $(shell arch)
ifeq ($(ARCH),armv7l)
	SRCDIR		:= src-arm
else
	SRCDIR		:= src
endif
.PHONY: all

# Targets
all: $(PROGS)

## Tests
$(PROGS): %: $(SRCDIR)/%.c
	$(CC) $(OUR_CFLAGS) -o $@ $<

install: chaiwala-v4l2fb
	$(INSTALL) -m755 -d $(DESTDIR)/$(BINDIR)
	$(INSTALL) -m755 $(PROGS) $(DESTDIR)/$(BINDIR)

uninstall:
	rm -f $(addprefix $(DESTDIR)/$(BINDIR),$(PROGS))
	rmdir -p --ignore-fail-on-non-empty $(addprefix $(DESTDIR),$(BINDIR))

clean:
	rm -f */*.o $(PROGS)
