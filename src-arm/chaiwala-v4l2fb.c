#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/videodev2.h>
#include <linux/fb.h>
#include <string.h>
#include <signal.h>
#include <dirent.h>

#include <linux/kd.h>

#define WIDTH 1024
#define HEIGHT 768
#define FRAMERATE 30
#define DEV_FS "/dev/"
#define V4L_DEV_PREFIX  "video"
#define FB_NAME DEV_FS "fb0"

static void relax(void)
{
	sigset_t s;
	int sig;

	sigemptyset(&s);
	sigaddset(&s, SIGINT);
	sigwait(&s, &sig);
}

int
filter_v4l_devices(const struct dirent *name)
{
	if (strncmp(name->d_name,
		    V4L_DEV_PREFIX,
		    strlen(V4L_DEV_PREFIX)) == 0)
		return 1;
	return 0;
}

int
find_v4l_device(int *fd_v4l)
{
	int n;
	char name[100];
	struct dirent **namelist;
	
	n = scandir(DEV_FS, &namelist, filter_v4l_devices, alphasort);

	while (n--) {
		snprintf (name, 20, "%s%s", DEV_FS, namelist[n]->d_name);
		if ((*fd_v4l = open(name, O_RDWR, 0)) < 0)
			continue;
		return 0;
	}
	return -1;
}

int
main(int argc, char **argv)
{
	struct v4l2_streamparm parm;
	int overlay = 1;
	struct v4l2_format fmt;
	struct v4l2_framebuffer fb_v4l2;
	int fd_fb, fd_v4l, vt_fd;
	int ret = 0;

	if ((vt_fd = open("/dev/tty0", O_RDWR)) < 0)
		exit(EXIT_FAILURE);

	if (ioctl(vt_fd, KDSETMODE, KD_GRAPHICS) < 0)
		exit(EXIT_FAILURE);

	if (find_v4l_device(&fd_v4l) < 0)
		exit(EXIT_FAILURE);

	if ((fd_fb = open(FB_NAME, O_RDWR)) < 0)
		exit(EXIT_FAILURE);
	/* XXX: since we've got the fb open, we could check res instead
	   of hard coding it...
	*/
	if (ioctl(fd_fb, FBIOBLANK, VESA_NO_BLANKING) < 0)
		exit(EXIT_FAILURE);

	fmt.type = V4L2_BUF_TYPE_VIDEO_OVERLAY;
	fmt.fmt.win.w.top = 0;
	fmt.fmt.win.w.left = 0;
	fmt.fmt.win.w.width = WIDTH;
	fmt.fmt.win.w.height = HEIGHT;

	parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	parm.parm.capture.timeperframe.numerator = 1;
	parm.parm.capture.timeperframe.denominator = FRAMERATE;
	parm.parm.capture.capturemode = 0;

	if (ioctl(fd_v4l, VIDIOC_S_PARM, &parm) < 0)
		exit(EXIT_FAILURE);

	if (ioctl(fd_v4l, VIDIOC_S_FMT, &fmt) < 0)
		exit(EXIT_FAILURE);

	memset(&fb_v4l2, 0, sizeof(fb_v4l2));

	fb_v4l2.fmt.width = WIDTH;
	fb_v4l2.fmt.height = HEIGHT;
	/* XXX: really should be using kms or something to get a buffer! */
	fb_v4l2.base = (void *)(0x40000000 + (1024 * 1024));
	fb_v4l2.fmt.sizeimage = WIDTH * HEIGHT * 4;
	fb_v4l2.fmt.pixelformat = V4L2_PIX_FMT_BGR32;
	fb_v4l2.flags = V4L2_FBUF_FLAG_OVERLAY;
	if (ioctl(fd_v4l, VIDIOC_S_FBUF, &fb_v4l2) < 0)
		exit(EXIT_FAILURE);

	if (ioctl(fd_v4l, VIDIOC_OVERLAY, &overlay) < 0)
		exit(EXIT_FAILURE);

	relax();

	close(fd_v4l);
	close(fd_fb);
	return ret;
}
